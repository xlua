--
-- this short utility attempts to make educated guess
-- about how to talk the x11 protocol based on the input
-- xproto.xml file. i've tried proper XSLT for lua first
-- and ended up like this ... its far more flexible anyway.
--

---------------------------------------------------------
-- misc util 
---------------------------------------------------------

local tconcat = table.concat
-- insert to table, if not nil
local tinsert = function(t,v)
	if (v) then table.insert(t,v) end
end
-- quote string
local function quote(s)
	return '"'..s..'"'
end

---------------------------------------------------------
-- naive, but trivial xml parser
---------------------------------------------------------

-- transcribe &escapes;, add more if necessary
local	htmltab = {
	amp = "&"
}

local function striphtml(s)
	return s:gsub("(%&[a-zA-Z]*%;)", function(s)
		return htmltab[s:sub(2,#s-1):lower()]
	end)
end

-- parse label attributes
local function parseargs(s)
	local arg = {}
	string.gsub(s, "([%w-]+)[ \t]*=[ \t]*([\"'])(.-)%2", function (w, _, a)
		arg[striphtml(w)] = striphtml(a)
	end)
	return arg
end

-- build tree
local function xml_parse(s)
	local stack = {}
	local top = {}
	tinsert(stack, top)
	local ni,c,label,xarg, empty
	local i, j = 1, 1
	while true do
		ni,j,c,label,xarg, empty = string.find(s, "<(%/?)(%w+)(.-)(%/?)>", i)
		if not ni then break end
		local text = string.sub(s, i, ni-1)
		if not string.find(text, "^%s*$") then
			tinsert(top, text)
		end
		if empty == "/" then  -- empty element tag
			tinsert(top, {label=label, xarg=parseargs(xarg), empty=1})
		elseif c == "" then   -- start tag
			top = {label=label, xarg=parseargs(xarg)}
			tinsert(stack, top)   -- new level
		else  -- end tag
			local toclose = table.remove(stack)  -- remove top
			top = stack[#stack]
			if #stack < 1 then
				error("nothing to close with "..label)
			end
			if toclose.label ~= label then
				error("trying to close "..toclose.label.." with "..label)
			end
			tinsert(top, toclose)
		end
	    i = j+1
	end
	local text = string.sub(s, i)
	if not string.find(text, "^%s*$") then
		tinsert(stack[stack.n], striphtml(text))
	end
	if #stack > 1 then
		error("unclosed "..stack[stack.n].label)
	end
	return stack[1]
end


---------------------------------------------------------
-- X11/XML madness begins here
---------------------------------------------------------

local	typfmt = {
	BOOL = "1",
	BYTE = "B",
	CARD8 = "B",
	CARD16 = "H",
	CARD32 = "I",
	INT16 = "h",
	INT32 = "I",
	INT8 = "b",
	void = "c",
	char = "c",
}

local	enums = {}
local	typfun = {}
local	errornum = {}
local	errorfun = {}
local	eventnum = {}
local	eventfun = {}
local	reqfun = {}
local	reqnum = {}

-- convert values with fieldrefs, operators etc to lua
local	function process_value(tlist,...)
	assert(tlist)
	local optab
	local level=1
	local lstack = {{}}
	local expr
	optab = {
		op = function(oa)
			
			level=$+1
			lstack[level] = {}
			-- force integer division XXX todo
			--if (oa.op == "/") then oa.op = "//" end
			lstack[level].op = oa.op
			return optab
		end,
		fieldref = function()  return {
			_data = function(d)
				tinsert(lstack[level], "v."..d)
			end,
		} end,
		value = function()  return {
			_data = function(d)
				tinsert(lstack[level], d)
			end,
		} end,
		bit = function() return {
			_data = function(d)
				tinsert(lstack[level], tostring((1<<tonumber(d))))
			end,
		} end,
		_exit = function()
			if (level==1) then
				local e= tconcat(lstack[1])
				if (tonumber(e)) then
					-- avoid function if its just constant
					tinsert(tlist,tonumber(e))
				else
					tinsert(tlist,"function(v) return (".. e..") end")
				end
				-- append args
				for i=1,#arg do
					tinsert(tlist,arg[i])
				end
				--tinsert(tlist,term)
			else
				-- we're deep in expression, reverse the polish to natural notation
				assert(lstack[level].op)
				local expr = tconcat(lstack[level], lstack[level].op)
				level=$-1
				tinsert(lstack[level], '('..expr..')')
			end
		end,
	}
	return optab
end


-- generate struct/union/request/response etc field parsers
local fields
fields = function(finishfunc)
	local ofmt = {}
	local onames = {narg=0}
	local prepend = {}
	local rfmt = ""
	local rnames = {}
	return {
		field = function(fa)
			tinsert(ofmt,typfmt[fa.type])
			tinsert(onames,quote(fa.name))
			onames.narg=$+1
		end,
		pad = function(fa)
			tinsert(ofmt,tonumber(fa.bytes).."x")
		end,
		list = function(fa,empty)
			local tf = typfmt[fa.type]
			tinsert(ofmt, "#"..tf)
			onames.narg=$+1
			if empty then
				tinsert(onames, "#arg-"..onames.narg)
				if (tf == "$") then
					tinsert(onames, "typfun."..fa.type)
				end
				tinsert(onames, quote(fa.name))
				return
			end
			return process_value(onames, (tf=="$" and "typfun."..fa.type), quote(fa.name))
		end,
		valueparam = function(vpa)
			local vptype = vpa['value-mask-type']
			local vpname = vpa['value-mask-name']
			local vplname = vpa['value-list-name']

			tinsert(ofmt, typfmt[vptype])
			tinsert(onames, quote(vpname))
			onames.narg=$+1

			tinsert(ofmt, '#I')
			tinsert(onames, 'function() return arg[#arg] end')
			tinsert(onames, quote(vplname))
			onames.narg=$+1

			tinsert(prepend, 'chkvlist(arg,"'..vpname..'")')
		end,
		exprfield = function(exa)
			tinsert(prepend, 'exprfield(arg,"'..exa.name..'",'..onames.narg..",")
			tinsert(ofmt, typfmt[exa.type])
			tinsert(onames, quote(exa.name))
			onames.narg=$+1
			return process_value(prepend, ")")
		end,
		reply = function(rplya)
			return fields(function(ofmt,onames)
				rfmt = ofmt
				rnames = onames
			end)
		end,
		_exit = function()
			finishfunc(ofmt,onames,prepend,rfmt,rnames)
		end
	}
end

local	function copy_type(a,b)
	typfmt[a] = typfmt[b]
	typfun[a] = typfun[b]
end

local parenthooks


-- the most interesting stuff
parenthooks = { xcb = function() return {
	xidtype = function(a) typfmt[a.name] = "I" end,
	xidunion = function(a)
		return {
			type = function(ta)
				return {
					_data = function(d)
						copy_type(a.name, d)
					end
				}
			end,
		}
	end,
	typedef = function(a)
		copy_type(a.newname,a.oldname)
	end,
	enum = function(a)
		local nlist = {}
		local elist = {}
		enums[a.name] = {}
		return {
			item = function(ea)
				tinsert(nlist,ea.name)
				return process_value(elist)
			end,
			_exit = function()
				for i,nam in ipairs(nlist) do
					enums[a.name][nam] = elist[i]
				end
			end
		}
	end,
	struct = function(a)
		--local ofmt = ""
		--local onames = {}
		typfmt[a.name] = "$"
		return fields(
		function(ofmt,onames)
			typfun[a.name] =
				'function(a,s,arg) return s:format("'..tconcat(ofmt)..
				'",arg,'..tconcat(onames,",")..') end'
		end)
	end,
	union = function(a)
		typfmt[a.name] = "$"
		return fields(
			function(ofmt,onames)
				typfun[a.name] = 'function(a,s,arg) return s:format("|'..tconcat(ofmt,"|")..
				 '",arg,'..tconcat(onames,",")..')end'
		end)
	end,
	event = function(eva)
		eventnum[eva.name] = eva.number
		return fields(
			function(ofmt,onames)
				eventfun[eva.name] = 'function(s,arg) return s:format("'..tconcat(ofmt)..
				'",arg,'..tconcat(onames,",")..')end'
		end)
	end,
	eventcopy = function(eva)
		eventnum[eva.name] = eva.number
		eventfun[eva.name] = "eventfun."..eva.ref -- eventfun[eva.ref]
	end,
	error = function(era)
		errornum[era.name] = era.number
		return fields(function(ofmt,onames)
			errorfun[era.name] = 'function(s,arg) return s:format("'..tconcat(ofmt)..
			'",arg,'..tconcat(onames,",")..')end'
		end)
	end,
	errorcopy = function(era)
		errornum[era.name] = era.number
		errorfun[era.name] = "errorfun."..era.ref -- eventfun[eva.ref]
	end,
	request = function(ra)
		--reqnum[ra.name] = ra.opcode
		return fields(
		function(fmt,names,prepend,rfmt,rnames)
			reqfun[ra.name] = 'function(s,arg) '..tconcat(prepend,"\n")..
			'xrequest('..ra.opcode..',s:format("'..tconcat(fmt)..'",arg'..
			(names[1] and ',' or "")..tconcat(names,",").."))"..
			(rfmt[1] and ('return s:format("'..tconcat(rfmt)..
			'", xreply(), '..tconcat(rnames,",")..') end') or "end")
		end)
	end
} end }

-- here comes xml diving
local xml_dive
xml_dive = function(tree,hooks)
	for idx,branch in ipairs(tree) do
		if type(branch) =="string" then
			if (hooks._data) then hooks._data(branch) end
			continue
		end
		local lab = branch.label
		local args = branch.xarg
		-- no valid label, ignore!
		if not lab then continue end
		if not hooks or not hooks[lab] then
			print("WARNING: unhandled label "..lab)
			continue
		end
		-- recurse deeper
		xml_dive(branch, hooks[lab](args, branch.empty))
	end
	-- exit hook while leaving the branch
	if hooks and hooks._exit then hooks._exit() end
end


---------------------------------------------------------
-- dumpers, define your own out() function
---------------------------------------------------------

local function dumpconst(t,tn)
	out(tn.."={")
	for k,v in pairs(t) do
		for a,b in pairs(v) do
			out(k..a.."="..b..",\n")
		end
	end
	out("}\n")
end

local function dumpfun(t,tn)
	local save={}
	out(tn.."={")
	for k,v in pairs(t) do
		if v:sub(1,#tn) != tn then
			out(k.."="..v..",\n")
		else
			tinsert(save, tn.."."..k.."="..v)
		end
	end
	out("}\n")
	out(tconcat(save,"\n").."\n")
end

local function dumpval(t,tn)
	out(tn.."={")
	for k,v in pairs(t) do
		if (tonumber(v)) then
			v = tonumber(v)
		elseif (type(v) == "string") then
			v = '"'..v..'"'
		end
		out(k.."="..v..",\n")
	end
	out("}\n")
end

---------------------------------------------------------
-- main()
---------------------------------------------------------
function out(a)
	io.stderr:write(a)
end

local a = io.open("xproto.xml"):read("*a")
r=xml_parse(a)
xml_dive(r,parenthooks)

dumpconst(enums,"const")

dumpval(typfmt,"typfmt")
dumpfun(typfun,"typfun")

dumpval(errornum,"errornum")
dumpfun(errorfun,"errorfun")

dumpval(eventnum,"eventnum")
dumpfun(eventfun,"eventfun")

dumpval(reqnum,"reqnum")
dumpfun(reqfun,"reqfun")

