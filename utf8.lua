function to_unicode(utf8)
	local utab = {}
	local l = #utf8
	local i=1
	while i <= l do
		local b = utf8:byte(i)
		i=$+1
		if b & 0x80 == 0 then
			table.insert(utab, b)
			continue
		elseif b & 0xe0 == 0xc0 then
			b = $ & 31
			cont = 1
		elseif b & 0xf0 == 0xe0 then
			b = $ & 15
			cont = 2
		elseif b & 0xfc == 0xf8 then
			b = $ & 7
			count = 3
		elseif b & 0xf7 == 0xf0 then
			b = $ & 3
			cont = 4
		elseif b & 0xfe == 0xfd then
			b = $ & 1
			cont = 5
		end
		while cont > 0 do
			b = ($ << 6) | (utf8:byte(i) & 0x3f)
			i=$+1
			cont=$-1
		end
		table.insert(utab,b)
	end
	return utab
end

