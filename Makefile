all: x

clean:
	make -C lua clean
	rm -f *.o x

lua/src/liblua.a:
	make -C lua ansi

x: x.o lua/src/liblua.a
	cc -o x -lm -Wall -lX11 -lefence x.o lua/src/liblua.a

x.o: x.c
	cc -o x.o -O0 -ggdb -Wall -I/usr/X11R6/include -Ilua/src -c x.c

