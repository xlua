desktops = { "stat", "term", "ssh", "mp3", "www", "mail", "im", }


-- map ModX to more sane names
x.AltMask = x.Mod1Mask
x.WinMask = x.Mod4Mask

-- take the key name and translate it to desktop/window index
local	function winswitch(key,mod)
	return cwm.switch_window(tonumber(key:sub(1,-1)))
end
local	function deskswitch(key,mod)
	return cwm.switch_desktop(tonumber(key:sub(1,-1)))
end

keymap = {
	-- spawn terminal
	["Win+T"]=true,

	-- local window switching
	["Alt+F1"]=winswitch,
	["Alt+F2"]=winswitch,
	["Alt+F3"]=winswitch,
	["Alt+F4"]=winswitch,
	["Alt+F5"]=winswitch,
	["Alt+F6"]=winswitch,
	["Alt+F7"]=winswitch,
	["Alt+F8"]=winswitch,
	["Alt+F9"]=winswitch,
	["Alt+F10"]=winswitch,
	["Alt+F11"]=winswitch,
	["Alt+F12"]=winswitch,
	["Alt+Tab"]=function()
		-- save current window number
		local prev = cwm.current_desktop.current_window
		-- switch to the previous
		cwm.switch_window(cwm.current_desktop.prev_window)
		-- and save our as previous
		cwm.current_desktop.prev_window = prev
	end

	-- global context window switching
	["Win+F1"]=deskswitch,
	["Win+F2"]=deskswitch,
	["Win+F3"]=deskswitch,
	["Win+F4"]=deskswitch,
	["Win+F5"]=deskswitch,
	["Win+F6"]=deskswitch,
	["Win+F7"]=deskswitch,
	["Win+F8"]=deskswitch,
	["Win+F9"]=deskswitch,
	["Win+F10"]=deskswitch,
	["Win+F11"]=deskswitch,
	["Win+F12"]=deskswitch,
	["Win+Tab"]=function()
		local prev = cwm.current_desktop.idx
		cwm.switch_desktop(cwm.prev_desktop)
		cwm.prev_desktop = prev
	end
}
