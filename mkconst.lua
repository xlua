#!/usr/bin/lua

function getlines(f)
	return io.open("/usr/include/X11/"..f..".h"):lines()
end

print("atom = {")
for l in getlines("Xatom") do
	local n,v = l:match("^#define XA_([^ ]*)[^0-9]*([0-9]*).*")
	if (n and v) then
		print(n.."="..v..",")
	end
end
print("}\nx=x or {}");

for l in getlines("X") do
	local n,v = l:match("^#define *([^ \t]*)[ \t]*(.*)") do
		if n and n != "" then
			if n=="X_H" then continue end
			v=v:gsub("%/%*.*", "")
			v=v:gsub("([0-9])L", "%1")
			v=v:gsub("%(int%)", "x.")
			-- bitshift
--			if v:find("1<<") then
--				v=1<<v:match("%(1<<([0-9]*)")
--			end
			print("x."..n.."="..v)
		end
	end
end

print("xcursor={")
for l in getlines("cursorfont") do
	local n,v = l:match("^#define XC_([^ ]*)[^0-9]*([0-9]*).*")
	if (n and v) then
		print(n.."="..v..",")
	end
end
print("}")
