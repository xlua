require "xconst"

-- windows table
cwm={
	windows={}
	winkeys={}
}

function	cwm.init()
	-- open root window
	cwm.root=x.init()

	-- intern useful atoms
	for _,v in pairs { "_NET_SUPPORTED", "_NET_WM_NAME", "WM_STATE" } do if not atom[v] then atom[v] = x.intern_atom(v) end end

	-- utility window calls
	local rt = getmetatable(cwm.root)
	rt.getwmstate = function(win)
		local res = win:get_property(atom.WM_STATE)
		if res then return res[0] end
	end
	rt.setwmstate = function(win,data)
		assert(data)
		win:set_property(atom.WM_STATE,nil, data, 0)
	end
end

-------------------------------------------------------
-- steal windows
-------------------------------------------------------
function	dt(t)
	for k,v in pairs(t) do
		print(k,v)
	end
end

function cwm.add_window(win,attr)
	-- shouldnt happen
	assert(attr)
	if (cwm.windows[win]) then return end

	cwm.windows[win] = attr
	cwm.windows[win].old_bw = attr.border_width
--	win:configure{border_width=5}
	win:set_attributes{border_pixel=x.color('#ff0000')}
end

function	cwm.setup()
	cwm.root:set_attributes {
		override_redirect=true,
		background_pixmap=x.ParentRelative,
		event_mask=
			x.SubstructureRedirectMask|
			x.SubstructureNotifyMask|
			x.ButtonPressMask|
			x.EnterWindowMask|
			x.LeaveWindowMask|
			x.StructureNotifyMask
	}
end

function cwm.scan()
	local wins = x.query_tree()
	for _,win in ipairs(wins) do
		local a=win:get_attributes()

		-- ignore overriden windows
		if a.override_redirect then
			continue
		end
		if a.map_state == x.IsViewable or win:getwmstate() == atom.IconicState then
			cwm.add_window(win,a)
		end
	end
end

function	cwm.grabkeys(win,map)
	for k,func in pairs(map) do
		local mt={}
		local modmask=0
		for e in string.gmatch(k, "[^+-]*") do
			if e == "" then continue end
			table.insert(mt,e)
		end
		for i=1,#mt-1 do
			modmask = $ | x[mt[i].."Mask"]
		end
		local key = x.keysym2keycode(x.string2keysym(mt[#mt]))

		-- grab the keycode
		win:grab_key(key, modmask)

		-- and register handler
		cwm.winkeys[win] = $ or {}
		cwm.winkeys[win][modmask] = $ or {}
		cwm.winkeys[win][modmask][key] = func
	end
end

cwm.init()
cwm.grabkeys(cwm.root,keymap)

cwm.setup()
cwm.scan()

-- prepare input event mask
while true do
	print("waiting for next event")
	local ev=x.get_events()
	if ev then
		print("GOT EVENT", ev.type)
		dt(ev)
	else
		print("nil event?")
	end
end

