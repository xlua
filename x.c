#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include <stdio.h>
#include <stdlib.h>

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
#include <assert.h>
#include "shader.c"
#include <string.h>
#include <stdint.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

extern char **environ;
GC gc;
Window root;
Display *dpy;
Colormap cmap;
int scr,w,h;
int xfd;
int maxfd=-1;

fd_set rfds,trfds;

XFontSet global_fontset = NULL;

typedef struct {
	XFontSet set;
	XFontStruct *xfont;
	int ascent;
	int descent;
	int height;
} myfont_t;

typedef struct {
	int infd, outfd;
	pid_t pid;
} process_t;

#define CLEAR(x) memset(&x, 0, sizeof(x))
#define MAXFD(fd) (maxfd=fd>maxfd?fd:maxfd)
#define CHECKSTR(n) luaL_checkstring(L, n)
#define OPTSTR(n,opt) luaL_optstring(L, n, opt)
#define CHECKBOOL(n) lua_toboolean(L, n)
#define OPTBOOL(n,v) CHECKBOOL(n)
#define CHECKINT(n) luaL_checknumber(L, n)
#define OPTINT(n,o) luaL_optnumber(L, n, o)
#define TOINT(n) lua_tointeger(L, n)
#define TOCOLOR(n) lua_tointeger(L, n)
#define PUSHNUM(n) lua_pushnumber(L, n)

#define CHECKMETA(n,name) checkmeta(L,n,name,name " expected")
//(((strcmp(getmeta(L,n), name))?luaL_argerror(L,n,name " expected"):0),lua_touserdata(L,n))

/* pointer types */
#define CHECKIMG(n) (*(XImage **) CHECKMETA(n,"<image>"))
#define CHECKFONT(n) ((myfont_t *) CHECKMETA(n,"<font>"))

/* xid types */
#define CHECKPIX(n) (*(Pixmap *) CHECKMETA(n, "<pixmap"))
#define CHECKWIN(n) (*(Window *) CHECKMETA(n,"<window>"))
#define CHECKCURSOR(n) (*(Cursor *) CHECKMETA(n, "<cursor>"))
#define OPTFONT(n,opt) (strcmp(getmeta(L,n),"<font>")?opt:(myfont_t *) lua_touserdata(L,n))
#define OPTPIX(n,opt) (strcmp(getmeta(L,n),"<pixmap>")?opt:(*(Pixmap *) lua_touserdata(L, n)))

#define CHECKDRAW(n) checkdraw(L,n)
#define CHECKATOM(a) CHECKINT(a)
#define OPTATOM(a,b) OPTINT(a,b)

#define METHOD(n) int n(lua_State *L)

#define ADOPTWIN(x) newxid(L, x, "<window>", NULL)
#define MKPIX(x) newxid(L, x, "<pixmap>", XFreePixmap);
#define MKWIN(x) newxid(L, x, "<window>", XDestroyWindow);
#define MKATOM(x) lua_pushnumber(L, x)
#define MKCURSOR(x) newxid(L, x, "<cursor>", XFreeCursor);

#define SET_READ(fd) FD_SET(fd, &rfds)
#define UNSET_READ(fd) FD_CLR(fd, &rfds)

#define FIXWHXY() \
	if (w < 0 || h < 0) { \
		x=y=0; \
		XGetGeometry(dpy, win, NULL, NULL, NULL, &w, &h, NULL, NULL); \
	}
#define FIXWH() \
	if (w < 0 || h < 0) \
		XGetGeometry(dpy, win, NULL, NULL, NULL, &w, &h, NULL, NULL);

#define SETI(n) { lua_pushnumber(L, n); lua_setfield(L, -2, #n); }
#define SETW(n) { ADOPTWIN(n); lua_setfield(L, -2, #n); }
#define SETA(n) { MKATOM(n); lua_setfield(L, -2, #n); }
#define SETB(n) { lua_pushboolean(L, n); lua_setfield(L, -2, #n); }

#define ATTRI(n) { lua_pushnumber(L, a.n); lua_setfield(L, -2, #n); }
#define ATTRW(n) { ADOPTWIN(a.n); lua_setfield(L, -2, #n); }
#define ATTRA(n) { MKATOM(a.n); lua_setfield(L, -2, #n); }
#define ATTRB(n) { lua_pushboolean(L, a.n); lua_setfield(L, -2, #n); }

#define EVI(u,n) { lua_pushnumber(L, ev.x##u.n); lua_setfield(L, -2, #n); }
#define EVW(u,n) { ADOPTWIN(ev.x##u.n); lua_setfield(L, -2, #n); }
#define EVA(u,n) { MKATOM(ev.x##u.n); lua_setfield(L, -2, #n); }
#define EVB(u,n) { lua_pushboolean(L, ev.x##u.n); lua_setfield(L, -2, #n); }


#define GETP(field,m) lua_getfield(L, idx, #field); if (!lua_isnil(L, -1)) { wa->field = OPTPIX(-1,CHECKINT(-1)); mask |= CW##m; }; lua_pop(L, 1);
#define GETI(field,m) lua_getfield(L, idx, #field); if (!lua_isnil(L, -1)) { wa->field = CHECKINT(-1); mask |= CW##m; } lua_pop(L, 1);
#define GETB(field,m) lua_getfield(L, idx, #field); if (!lua_isnil(L, -1)) { wa->field = lua_toboolean(L, -1); mask |= CW##m; } lua_pop(L, 1);
#define GETC(field,m) lua_getfield(L, idx, #field); if (!lua_isnil(L, -1)) { wa->field = CHECKCURSOR(-1); mask |= CW##m; } lua_pop(L, 1);

#define DEBUG(fmt...) { fprintf(stderr, "%d %s(): ", __LINE__, __func__); fprintf(stderr, fmt); fprintf(stderr, "\n"); fflush(stderr); }
//#define DEBUG(fmt...)

char *getmeta(lua_State *L, int n)
{
	const char *s;
	//assert(n>0);
	//DEBUG("top %d", lua_gettop(L));
	if (!lua_getmetatable(L, n)) return "";
	//DEBUG("top %d", lua_gettop(L));
	lua_rawget(L, LUA_REGISTRYINDEX);
	s = lua_tostring(L, -1);
	//DEBUG("getmeta(%d)=%s",n,s);
	lua_pop(L, 1);
	if (!s) s="";
	//DEBUG("top %d", lua_gettop(L));
	return (char*)s;
}

void	*checkmeta(lua_State *L, int n, char *name, char *expect)
{
	void *d;
	//DEBUG("top %d", lua_gettop(L));
	if (strcmp(getmeta(L,n), name))
		luaL_argerror(L,n,expect);
	d = lua_touserdata(L, n);
	//DEBUG("top %d", lua_gettop(L));
	assert(d!=NULL);
	return d;
	
}

void	kill_xid(lua_State *L, XID xid)
{
	lua_getfield(L, LUA_REGISTRYINDEX, "destruct");
	lua_pushnil(L);
	lua_rawseti(L, -2, xid);

	lua_getfield(L, LUA_REGISTRYINDEX, "xid");
	lua_pushnil(L);
	lua_rawseti(L, -2, xid);
}


/* create new XID pointer */
/* must return from lua function! xids may be gcable by their destructors (xid table is weak) */
int newxid(lua_State *L, XID xid, const char *name, void *destruct)
{
	XID *xidp;
	if (!xid) { lua_pushnil(L); return 1; };
	assert(xid!=0);
	DEBUG("%d %p", lua_gettop(L), destruct);
	lua_getfield(L, LUA_REGISTRYINDEX, "xid");
	lua_rawgeti(L, -1, xid);
	if (!lua_isnil(L, -1)) {
		DEBUG("cache hit");
		XID *x = lua_touserdata(L, -1);
		lua_remove(L, -2);
		DEBUG("%d %p", lua_gettop(L), destruct);
		if (*x) return 1; /* got valid one */
	}
	lua_pop(L, 1);
	/* -1 xid table */
	xidp = lua_newuserdata(L, sizeof(XID));
	*xidp = xid;

	/* -2 xid table, -1 = udata */
	/* set meta */
	lua_getfield(L, LUA_REGISTRYINDEX, name);
	assert(!lua_isnil(L, -1));
	lua_setmetatable(L, -2);

	/* set to xid */
	if (destruct) {
		lua_getfield(L, LUA_REGISTRYINDEX, "destruct");
		lua_pushvalue(L, -2);
		lua_pushlightuserdata(L, destruct);
		lua_rawset(L, -3);
		lua_pop(L, 1);
	}

	lua_pushvalue(L, -1);
	lua_rawseti(L, -3, xid);

	lua_remove(L, -2);
	DEBUG("%d", lua_gettop(L));
	assert(lua_touserdata(L, -1)==xidp);
	return 1;
}

/* plain pointers, always gcable */
int newptr(lua_State *L, void *p, const char *name)
{
	*((void **) lua_newuserdata(L, sizeof(void *))) = p;
	lua_getfield(L, LUA_REGISTRYINDEX, name);
	lua_setmetatable(L, -2);
	return 1;
}

void *newudata(lua_State *L, size_t size, const char *name)
{
	void *p = lua_newuserdata(L, size);
	memset(p, 0, size);
	lua_getfield(L, LUA_REGISTRYINDEX, name);
	lua_setmetatable(L, -2);
	return p;
}



Drawable checkdraw(lua_State *L, int n)
{
	Drawable *d = NULL;
	const char *s;

	assert(n>0);
	s = getmeta(L, n);
	if (!strcmp(s, "<window>") || !strcmp(s,"<pixmap>"))
		d = lua_touserdata(L, n);
		return *d;
	return luaL_argerror(L,n,"<window> or <pixmap> expected");
}

void	process_kill(lua_State *L, process_t *p)
{
	if (p->infd>0) { close(p->infd); UNSET_READ(p->infd); p->infd = -1; };
	if (p->outfd>0) { close(p->outfd); p->outfd = -1; };
	if (p->pid>0) {
		/* still running? */
		if (waitpid(p->pid, NULL, WNOHANG)>0) {
			/* make it less so .. */
			kill(-p->pid, SIGKILL); 
			waitpid(p->pid, NULL, 0);
		}
		p->pid = -1;
	}

	/* remove from context table */
	lua_getfield(L, LUA_REGISTRYINDEX, "<spawn.context>");
	lua_pushuserdata(L, p);
	lua_pushnil(L);
	lua_rawset(L, -3);
	lua_pop(L, 1);
}


/* process incoming reads on spawned processes (infd) */
METHOD(read_processes)
{
	int n;
	DEBUG("top=%d",lua_gettop(L));
	/* find what process needs a read */
	lua_getfield(L, LUA_REGISTRYINDEX, "<spawn.context>");
	assert(!lua_isnil(L, -1));
	/* key is process, value is table with { line = "", func = {} } */
	lua_pushnil(L);
	DEBUG("top=%d",lua_gettop(L));
	while ((n=lua_next(L, -2))) {
		process_t *pr = lua_touserdata(L, -2); /* key is p */
		if (!pr) { lua_pop(L, 1); continue; };
		DEBUG("process %p %d",pr,n);
		/* alive and input for us */
		if (pr->infd > 0 && (FD_ISSET(pr->infd, &trfds))) {
			char readbuf[8192];
			char *p, *nl;
			int got = read(pr->infd, readbuf, sizeof(readbuf));
			/* resolve handler function */
			/* something's wrong, kill the process */
			if (got == 0 || (got < 0 && errno != EAGAIN)) {
				lua_getfield(L, -1, "func"); /* call func */
				lua_pushvalue(L, -3); /* process */
				lua_pushvalue(L, -3); /* context */
				lua_pushnil(L);	      /* line */
				lua_pushstring(L, "error"); /* reason */
				lua_pushnumber(L, errno); /* errno */
				/* process,context,line,errreason,errno */
				lua_call(L,5,0);
				process_kill(L, pr);
				lua_pop(L, 1); /* remove val */
				continue;
			}
			/* seems good, append line */
			lua_getfield(L, -2, "line");

			/* found newlines? */
			p = readbuf;
			while ((nl = memchr(p, got, '\n'))) {
				int np = 1; /* shift from nl to next p */
				if (nl > p && nl[-1] == '\r') { nl--; np++; };
				lua_getfield(L, -1, "func"); /* for call */
				lua_pushvalue(L, -3); /* process */
				lua_pushvalue(L, -3); /* context */
				/* first occurance, concatenate */
				if (p == readbuf) {
					lua_getfield(L, -2, "line");
					lua_pushlstring(L, p, nl-p);
					lua_concat(L, 2); /* -> results single line */
				} else lua_pushlstring(L, p, nl-p);
				lua_call(L, 3, 0);
				p += np;
			}
			/* in case we didn't process everything, save the line */
			lua_pushlstring(L, p, (readbuf+got)-p);
			lua_setfield(L, -2, "line");
		}
		lua_pop(L, 1); /* remove val */
	}
	return 0;
}

/*
 * x.spawn(cmd,func)
 * x.spawn(cmd,{func=fun,argv={...},envp={...}})
 * */

METHOD(x_spawn)
{
	int i;
	int pipein[2];
	int pipeout[2];
	const char *scmd = CHECKSTR(1);
	const char **env = (const char**)environ;
	const char **arg;
	process_t *pr;
	lua_settop(L, 2);
	if (lua_isfunction(L, 2)) {
		lua_newtable(L);
		lua_pushvalue(L, -2);
		lua_setfield(L, -2, "func");
		lua_replace(L, 2);
	} else if (!lua_istable(L, 2)) {
		return luaL_argerror(L, 2, "function or table expected as argument");
	}


	lua_getfield(L, -1, "arg");
	if (lua_isnil(L, -1)) {
		/* no argv - create new one */
		lua_pop(L, 1);
		lua_newtable(L);
		lua_pushliteral(L, "/bin/sh"); lua_rawseti(L, -2, 1);
		lua_pushliteral(L, "-c"); lua_rawseti(L, -2, 2);
		lua_pushvalue(L, 1); lua_rawseti(L, -2, 3);
		scmd = "/bin/sh";
	}
	/* construct real argv vector */
	arg = alloca((lua_objlen(L, -1)+1)*sizeof(char *));
	for (i = 1; i <= lua_objlen(L, -1); i++) {
		lua_rawgeti(L, -1, i);
		arg[i] = lua_tostring(L, -1);
		lua_pop(L, 1);
	}
	arg[i] = NULL;
	lua_pop(L, 1);


	/* construct real envp vector */
	lua_getfield(L, -1, "env");
	if (!lua_isnil(L, -1)) {
		env = alloca((lua_objlen(L, -1)+1)*sizeof(char*));
		for (i = 1; i <= lua_objlen(L, -1); i++) {
			lua_rawgeti(L, -1, i);
			env[i] = lua_tostring(L, -1);
			lua_pop(L, 1);
		}
		env[i] = NULL;
	}

	/* scmd, env, arg should be ready */
	pr = newudata(L, sizeof(pr[0]), "<process>");
	pipe(pipein);
	pipe(pipeout);
	MAXFD(pipein[0]); MAXFD(pipeout[0]); MAXFD(pipein[1]); MAXFD(pipeout[1]);
	pr->infd = pipein[0];
	pr->outfd = pipeout[1];
	pr->pid = fork();
	if (pr->pid > 0) {
		int n,ret=-1;
		/* parent */
		SET_READ(pr->infd);
		close(pipein[1]);
		close(pipeout[0]);
		do {
			n = waitpid(pr->pid, &ret, 0);
		} while ((n==-1) && errno==EINTR);
		if (n != pr->pid || ret <0) {
			process_kill(L,pr);
			return 0;
		}
		/* spawned ok? */
		return 1;
	}
	/* fork error */
	if (pr->pid < 0) return 0;
	/* child */
	close(pr->infd);
	close(pr->outfd);
	// XXX close on exec on everything by default?
	dup2(pipein[1], 1);
	dup2(pipeout[0], 0);
	for (i = 2; i <= maxfd; i++) close(i);
	execve(scmd, (char**)arg,(char**) env);
	exit(127);
	/* NEVER REACHED */
	return 0;
}


/********************************************************
 * X11 utils
 ********************************************************/
METHOD(read_x_event)
{
	XEvent ev;

	DEBUG("xevcent");
	XNextEvent(dpy, &ev);
	lua_newtable(L);
	lua_pushnumber(L, ev.type);
	lua_setfield(L, -2, "type");

	DEBUG("got type %d", ev.type);
	switch (ev.type) {
		case KeyPress: 
		case KeyRelease:
			EVI(key, state);
			EVI(key, keycode);
			break;
		case ButtonPress:
		case ButtonRelease:
			EVI(button, state);
			EVI(button, button);
			break;
		case MotionNotify:
			EVW(motion,window);
			EVW(motion,root);
			EVW(motion,subwindow);
			EVI(motion,time);
			EVI(motion,x);
			EVI(motion,y);
			EVI(motion,x_root);
			EVI(motion,y_root);
			EVI(motion,state);
			EVB(motion,is_hint);
			EVB(motion,same_screen);
			break;
		case EnterNotify:
		case LeaveNotify:
			DEBUG("enter notify?");
			EVW(crossing,window);
			EVW(crossing,root);
			EVW(crossing,subwindow);
			EVI(crossing,time);
			EVI(crossing,x);
			EVI(crossing,y);
			EVI(crossing,x_root);
			EVI(crossing,y_root);
			EVI(crossing,mode);
			EVI(crossing,detail);
			EVB(crossing,same_screen);
			EVB(crossing,focus);
			EVI(crossing,state);
			break;
		case FocusIn:
		case FocusOut:
			EVW(focus,window);
			EVI(focus,mode);
			EVI(focus,detail);
			break;
		case Expose:
			EVW(expose,window);
			EVI(expose,x);
			EVI(expose,y);
			EVI(expose,width);
			EVI(expose,height);
			EVI(expose,count);
			break;
		case CreateNotify:
			EVW(createwindow,parent);
			EVW(createwindow,window);
			EVI(createwindow,x);
			EVI(createwindow,y);
			EVI(createwindow,width);
			EVI(createwindow,height);
			EVI(createwindow,border_width);
			EVB(createwindow,override_redirect);
			break;
		case UnmapNotify:
			EVB(unmap,from_configure);
		case DestroyNotify:
			EVW(destroywindow,event);
			EVW(destroywindow,window);
			break;
		case MapNotify:
			EVW(map,event);
			EVW(map,window);
			EVW(map,override_redirect);
			break;
		case MapRequest:
			EVW(maprequest,parent);
			EVW(maprequest,window);
			break;
		case ReparentNotify:
			EVW(reparent,event);
			EVW(reparent,window);
			EVW(reparent,parent);
			EVW(reparent,x);
			EVW(reparent,y);
			EVB(reparent,override_redirect);
			break;
		case ConfigureRequest:
			EVW(configurerequest,parent);
			EVW(configurerequest,window);
			EVI(configurerequest,x);
			EVI(configurerequest,y);
			EVI(configurerequest,width);
			EVI(configurerequest,height);
			EVI(configurerequest,border_width);
			EVW(configurerequest,above);
			EVI(configurerequest,detail);
			EVI(configurerequest,value_mask);
			break;
		case ConfigureNotify:
			EVW(configure,event);
			EVW(configure,window);
			EVI(configure,x);
			EVI(configure,y);
			EVI(configure,width);
			EVI(configure,height);
			EVI(configure,border_width);
			EVW(configure,above);
			EVB(configure,override_redirect);
			break;
		case PropertyNotify:
			EVW(property,window);
			EVA(property,atom);
			EVI(property,time);
			EVI(property,state);
			break;
		default:
			DEBUG("something broke");
	}
	DEBUG("left event");
	return 1;
}


/* x.get_event(ms) */
METHOD(x_get_events)
{
	int got;
	struct timeval tv;
	double w = luaL_optnumber(L, 1, -1);
	tv.tv_sec = w/1000;
	tv.tv_usec = ((long)w%1000)*1000;

	DEBUG("stage");
	if (XPending(dpy))
		return read_x_event(L);

	DEBUG("stage");
	memcpy(&trfds, &rfds, sizeof(rfds));
	got = select(maxfd+1,&trfds,NULL,NULL,w<0?NULL:&tv);
	if (got <= 0) return 0;

	DEBUG("stage");
	read_processes(L);

	DEBUG("stage");
	/* something happening on X connection */
	if (FD_ISSET(xfd, &trfds)) {
		if (XPending(dpy))
			return read_x_event(L);
	}
	DEBUG("stage");
	return 0;
}


/* return w,h */
METHOD(x_geometry)
{
	PUSHNUM(DisplayWidth(dpy, scr));
	PUSHNUM(DisplayHeight(dpy, scr));
	return 2;
}


METHOD(x_string2keysym)
{
	KeySym ks = XStringToKeysym(CHECKSTR(1));
	if (ks == NoSymbol) return 0;
	lua_pushnumber(L,ks);
	return 1;
}

METHOD(x_keysym2string)
{
	char *s = XKeysymToString(CHECKINT(1));
	if (!s) return 0;
	lua_pushstring(L, s);
	return 1;
}

METHOD(x_keysym2keycode)
{
	KeyCode kc = XKeysymToKeycode(dpy, CHECKINT(1));
	if (!kc) return 0;
	lua_pushnumber(L,kc);
	return 1;
}

METHOD(x_keycode2keysym)
{
	KeySym ks = XKeycodeToKeysym(dpy, CHECKINT(1), 0);
	if (ks == NoSymbol) return 0;
	lua_pushnumber(L,ks);
	return 1;
}

/* if argument passed, set a new modmap */
METHOD(x_modmap)
{
	int i,j;
	XModifierKeymap *map;
	/* getting old one */
	if (lua_isnil(L, 1)) {
		map = XGetModifierMapping(dpy);
		lua_createtable(L, 8, 0);
		for (i = 0; i < 8; i++) {
			lua_newtable(L);
			for (j = 0; j < map->max_keypermod; j++) {
				KeyCode kc = map->modifiermap[i * map->max_keypermod + j];
				if (kc) {
					lua_pushnumber(L, kc);
				} else lua_pushnil(L);
				lua_rawseti(L, -2, j+1);
			}
			lua_rawseti(L, -2, i+1);
		}
		return 1;
	}

	/* setting new one */
	map = XNewModifiermap(8);
	for (i = 0; i < 8; i++) {
		lua_rawgeti(L, 1, i+1);
		lua_pushnil(L);
		while (lua_next(L, -2)) {
			XInsertModifiermapEntry(map, TOINT(-1), i);
			/* remove the "value", keep key */
			lua_pop(L, 1);
		}
	}
	XSetModifierMapping(dpy, map);
	XFreeModifiermap(map);
	lua_settop(L, 1);
	return 1;
}

METHOD(x_intern_atom)
{
	const char *name = CHECKSTR(1);
	int ifexist = CHECKBOOL(2);
	Atom a = XInternAtom(dpy, name, ifexist);
	if (a == None)
		return 0;
	MKATOM(a);
	return 1;
}

/* input either:
 * number - default cursor
 * pixmap, maskmask. fg, bg, x, y - pixmap cursor
 * font, mfont, char, maskchar, fg, bg - glyph cursor */ 
METHOD(x_create_cursor)
{
	XColor fg, bg;
	int x, y;
	myfont_t *f1, *f2;
	const char *s1, *s2;
	if (lua_isnumber(L, 1)) {
		return MKCURSOR(XCreateFontCursor(dpy, lua_tonumber(L, 1)));
	}
	if (!strcmp(getmeta(L, 1),"<pixmap>")) {
		Pixmap p1 = CHECKPIX(1);
		Pixmap p2 = OPTPIX(2, None);
		fg.pixel = OPTINT(3, 0xffffff);
		bg.pixel = OPTINT(4, 0);
		XQueryColor(dpy, cmap, &fg);
		XQueryColor(dpy, cmap, &bg);
		x = OPTINT(5, 0);
		y = OPTINT(6, 0);
		return MKCURSOR(XCreatePixmapCursor(dpy, p1, p2, &fg, &bg, x, y));
	}
	f1 = CHECKFONT(1);
	f2 = OPTFONT(2,f1);
	s1 = CHECKSTR(3);
	s2 = OPTSTR(4, s1);
	fg.pixel = OPTINT(5, 0xffffff);
	bg.pixel = OPTINT(6, 0);
	return MKCURSOR(XCreateGlyphCursor(dpy, f1->xfont->fid, f2->xfont->fid, s1[0], s2[0], &fg, &bg));
}

/* cur:recolor(fg,bg) */
METHOD(cur_recolor)
{
	Cursor c = CHECKCURSOR(1);
	int fgp = OPTINT(2,-1);
	int bgp = OPTINT(3,-1);
	XColor fg, bg;
	fg.flags=bg.flags=0;
	if (fgp>=0) {
		fg.pixel=fgp;
		XQueryColor(dpy, cmap, &fg);
	}
	if (bgp>0) {
		bg.pixel=bgp;
		XQueryColor(dpy, cmap, &bg);
	}

	XRecolorCursor(dpy,c,&fg,&bg);
	return 0;
}

METHOD(cur_destroy)
{
	Cursor cur = CHECKCURSOR(1);
	kill_xid(L, cur);
	XFreeCursor(dpy, cur);
	return 0;
}

/* load the font */
METHOD(x_load_font)
{
	const char *fn = CHECKSTR(1);
	char **missing=NULL;
	int n;
	myfont_t *myf = newudata(L,sizeof(*myf),"<font>");

//	myf->set = XCreateFontSet(dpy, fn, &missing, &n, &def);

	if (missing) XFreeStringList(missing);
	if (myf->set) {
		int i;
		XFontSetExtents *font_extents;
		XFontStruct **xfonts;
		char **font_names;
		font_extents = XExtentsOfFontSet(myf->set);
		n = XFontsOfFontSet(myf->set, &xfonts, &font_names);
		myf->ascent = myf->descent = 0;
		if (n) myf->xfont = *xfonts;
		for (i = 0; i < n; i++) {
#define MAX(a,b) ((a)>(b))?(a):(b)
			myf->ascent = MAX(myf->ascent, xfonts[i]->ascent);
			myf->descent = MAX(myf->descent,xfonts[i]->descent);
#undef MAX
		}
	} else {
		myf->xfont = XLoadQueryFont(dpy, fn);
		if (!myf->xfont) return 0;
		myf->ascent = myf->xfont->max_bounds.ascent;
		myf->descent = myf->xfont->max_bounds.descent;
	}
	myf->height = myf->ascent+myf->descent;
//	DEBUG("loaded font %d\n", myf->xfont->fid);
	return 1;
}

/* x.color("#ff0000") */
METHOD(x_color)
{
	XColor color;
	XAllocNamedColor(dpy,cmap,CHECKSTR(1),&color,&color);
	lua_pushnumber(L, color.pixel);
	return 1;
}

METHOD(x_fg)
{
	XSetForeground(dpy, gc, TOCOLOR(1));
	return 0;
}

METHOD(x_bg)
{
	XSetBackground(dpy, gc, TOCOLOR(1));
	return 0;
}

METHOD(x_set_font)
{
	myfont_t *f = CHECKFONT(1);
	if (f->set) {
		global_fontset = f->set;
		DEBUG("set global fontset");
	} else {
		global_fontset = NULL;
		XSetFont(dpy, gc, f->xfont->fid);
	}
	return 0;
}

METHOD(x_init)
{
	if (!dpy) {
		dpy = XOpenDisplay(NULL);
		if (!dpy) return 0;
		scr = DefaultScreen(dpy);
		root = RootWindow(dpy, scr);
		cmap = DefaultColormap(dpy, scr);
		xfd = ConnectionNumber(dpy);
		MAXFD(xfd);
		SET_READ(xfd);
	}
	gc=XCreateGC(dpy, root, 0, 0);
	ADOPTWIN(root);
	return 1;
}

METHOD(x_sync)
{
	XSync(dpy, lua_toboolean(L, 1));
	return 0;
}
METHOD(x_flush)
{
	XFlush(dpy);
	return 0;
}


METHOD(x_query_tree)
{
	Window d1, d2, *wins = NULL;
	unsigned int i, num;

	if (!XQueryTree(dpy, root, &d1, &d2, &wins, &num))
		return 0;
	lua_newtable(L);
	for (i = 0; i < num; i++) {
		ADOPTWIN(wins[i]);
		lua_rawseti(L, -2, i+1);
	}
	XFree(wins);
	return 1;
}

/********************************************************
 * Window utils
 ********************************************************/

static	unsigned long copy_attr(lua_State *L, int idx, XSetWindowAttributes *wa)
{
	unsigned long mask = 0;
	/* no table, no args */
	if (!lua_istable(L, idx)) return mask;


	GETP(background_pixmap, BackPixmap);
	GETI(background_pixel, BackPixel);
	GETP(border_pixmap, BorderPixmap);
	GETI(border_pixel, BorderPixel);
	GETI(bit_gravity, BitGravity);
	GETI(win_gravity, WinGravity);
	GETI(backing_store, BackingPixel);
	GETI(backing_planes, BackingPlanes);
	GETI(backing_pixel, BackingPixel);
	GETB(override_redirect, OverrideRedirect);
	GETB(save_under, SaveUnder);
	GETI(event_mask, EventMask);
	GETI(do_not_propagate_mask, DontPropagate);
	GETC(cursor, Cursor);
	return mask;
}

/* win:set_attributes{attr_tab} */
METHOD(win_set_attributes)
{
	Window win = CHECKWIN(1);
	unsigned long wmask;
	XSetWindowAttributes wa;

	wmask = copy_attr(L, 2, &wa);
	XChangeWindowAttributes(dpy, win, wmask, &wa);
	return 0;
}

/* create a (sub)window:
 * x.root:create_window(0,0[,w,h,border,{attributes...}])
 * */
METHOD(win_create_window)
{
	Window nw, win = CHECKWIN(1);
	int x = CHECKINT(2);
	int y = CHECKINT(3);
	unsigned int w = OPTINT(4,-1);
	unsigned int h = OPTINT(5,-1);
	int bw = OPTINT(6,0);
	unsigned long wmask;
	XSetWindowAttributes wa;

	FIXWHXY();

	wmask = copy_attr(L, 6, &wa);
	nw = XCreateWindow(dpy, root, x, y, w, h, bw, DefaultDepth(dpy, scr), CopyFromParent /*class*/, DefaultVisual(dpy, scr), wmask, &wa);
	if (!nw) return 0;
	return MKWIN(nw);
}


#define QRI(n) { lua_pushnumber(L, a.n); lua_setfield(L, -2, #n); }
#define QRW(n) { ADOPTWIN(a.n); lua_setfield(L, -2, #n); }
#define QRA(n) { MKATOM(a.n); lua_setfield(L, -2, #n); }
#define QRB(n) { lua_pushboolean(L, a.n); lua_setfield(L, -2, #n); }
METHOD(win_query_pointer)
{
	struct {
		Window root;
		Window win;
		int root_x, root_y;
		int x, y;
		unsigned int mod;
	} a;
	if (!XQueryPointer(dpy, CHECKWIN(1), &a.root, &a.win, &a.root_x, &a.root_y, &a.x, &a.y, &a.mod)) return 0;
	lua_newtable(L);
	QRW(root);
	QRW(win);
	QRI(root_x); QRI(root_y);
	QRI(x); QRI(y);
	QRI(mod);
	return 1;
}
#undef QRI
#undef QRW
#undef QRA
#undef QRB

METHOD(win_list_properties)
{
	int nret;
	Window win = CHECKWIN(1);
	int i;
	Atom *al = XListProperties(dpy, win, &nret);
	if (!al) return 0;
	lua_newtable(L);
	for (i = 0; i < nret; i++) {
		MKATOM(al[i]);
		lua_rawseti(L, -2, i+1);
	}
	XFree(al);
	return 1;
}

/* win:set_property(atom,atom,data1, data2...) */
METHOD(win_set_property)
{
	Window win = CHECKWIN(1);
	Atom a1 = CHECKATOM(2);
	Atom a2 = OPTATOM(3,2);
	int i;
	uint32_t *data;
	lua_settop(L, 3);
	data = alloca(lua_gettop(L)*sizeof(long));
	for (i = 0; i < lua_gettop(L)-3; i++)
		data[i] = lua_tonumber(L, i);
	XChangeProperty(dpy, win, a1, a2, 32, PropModeReplace, (unsigned char*)data, i);
	return 0;
}

/* win:get_property(atom[,atom2,off,len]) = {ilist}, real */
METHOD(win_get_property)
{
	unsigned char *p;
	Atom real;
	int format;
	Window win = CHECKWIN(1);
	Atom a1 = CHECKATOM(2);
	Atom a2 = OPTATOM(3,a1);
	unsigned long off, len,n,extra;
	int i;

	off = OPTINT(4, 0);
	len = OPTINT(5, 2);

	if (XGetWindowProperty(dpy, win, a1, off, len, False, a2, &real, &format, &n, &extra, &p) != Success) return 0;
//	DEBUG("nprop: %d %d", n, format);
	lua_newtable(L);
	for (i = 0; i < n; i += (format/8)) {
		switch (format) {
			case 8:
				lua_pushnumber(L, p[0]);
				break;
			case 16:
				lua_pushnumber(L, *(uint16_t *)(p));
				break;
			case 32:
				lua_pushnumber(L, *(uint32_t *)(p));
				break;
			default:
				XFree(p);
				luaL_error(L, "invalid property format");
		}
		lua_rawseti(L, -2, i+1);
	}
	lua_pushnumber(L, real);
	XFree(p);
	return 2;
}

METHOD(win_get_attributes)
{
	Window win = CHECKWIN(1);
	XWindowAttributes a;
	CLEAR(a);
	if (!XGetWindowAttributes(dpy, win, &a)) return 0;
	lua_newtable(L);
	ATTRI(x); ATTRI(y);
	ATTRI(width); ATTRI(height);
	ATTRI(border_width);
	ATTRI(depth);
	ATTRW(root);
	ATTRI(class);
	ATTRI(bit_gravity);
	ATTRI(win_gravity);
	ATTRI(backing_store);
	ATTRI(backing_planes);
	ATTRI(backing_pixel);
	ATTRB(save_under);
	ATTRB(map_installed);
	ATTRI(map_state);
	ATTRI(all_event_masks);
	ATTRB(override_redirect);
	return 1;
}

/* keycode, modmask */
METHOD(win_grab_key)
{
	XGrabKey(dpy, CHECKINT(2), OPTINT(3,AnyModifier), CHECKWIN(1), True, GrabModeAsync, GrabModeAsync);
	return 0;
}

#define BUTTONMASK              (ButtonPressMask|ButtonReleaseMask)
/* button, modifier, mask */
METHOD(win_grab_button)
{
	XGrabButton(dpy, CHECKINT(2), OPTINT(3,AnyModifier), CHECKWIN(1), False, OPTINT(4,BUTTONMASK), GrabModeAsync, GrabModeSync, None, None);
	return 0;
}

METHOD(win_ungrab_button)
{
	XUngrabButton(dpy, CHECKINT(2), OPTINT(3, AnyModifier), CHECKWIN(1));
	return 0;
}

METHOD(win_ungrab_key)
{
	XUngrabKey(dpy, TOINT(1), TOINT(2), CHECKWIN(1));
	return 0;
}

/* 
 * w:send_message(atom)
 */
METHOD(win_send_message)
{
	XEvent ev;
	Window win = CHECKWIN(1);
	ev.type = ClientMessage;
	ev.xclient.display = dpy;
	ev.xclient.window = win;
	ev.xclient.format = 32;
	ev.xclient.data.l[0] = CHECKATOM(2);
	ev.xclient.data.l[1] = CurrentTime;
	XSendEvent(dpy, win, False, NoEventMask, &ev);	
	return 0;
}

#define READI(n) (lua_getfield(L, 2, #n), luaL_checknumber(L, -1)); lua_pop(L, 1);
#define READW(n) (lua_getfield(L, 2, #n), CHECKWIN(-1)); lua_pop(L, 1);
#define READB(n) (lua_getfield(L, 2, #n), lua_toboolean(L,-1)); lua_pop(L, 1);

/* w:send_configure { table } */
METHOD(win_send_configure)
{
	XEvent ev;
	Window win = CHECKWIN(1);
	ev.type = ConfigureNotify;
	ev.xconfigure.display = dpy;
	ev.xconfigure.event = win;
	ev.xconfigure.window = win;
	ev.xconfigure.x = READI(x);
	ev.xconfigure.y = READI(y);
	ev.xconfigure.width = READI(width);
	ev.xconfigure.height = READI(height);
	ev.xconfigure.border_width = READI(border_width);
	ev.xconfigure.above = READW(above);
	ev.xconfigure.override_redirect = READB(override_redirect);
	XSendEvent(dpy, win, False, NoEventMask, &ev);
	return 0;
}
#undef READI
#undef READW
#undef READB

/* w:configure {table} */
METHOD(win_configure)
{
	XWindowChanges wc;
	XWindowChanges *wa = &wc;
	unsigned long mask = 0;
	int idx = 2;

	GETI(x, X);
	GETI(x, Y);
	GETI(width, Width);
	GETI(height, Height);
	GETI(border_width, BorderWidth);
	GETI(sibling, Sibling);
	GETI(stack_mode, StackMode);
	XConfigureWindow(dpy, CHECKWIN(1), mask, &wc);
	return 0;
}
METHOD(win_clear)
{
	Window win = CHECKWIN(1);
	int x = OPTINT(2, 0);
	int y = OPTINT(3, 0);
	unsigned int w = OPTINT(4, -1);
	unsigned int h = OPTINT(5, -1);
	int ex = OPTBOOL(6, 0);
	if (lua_gettop(L)==1) {
		XClearWindow(dpy, CHECKWIN(1));
		return 0;
	}
	FIXWHXY();
	XClearArea(dpy, win, x, y, w, h, ex);
	return 0;
}

/* explicitly kill window */
METHOD(win_destroy)
{
	Window win = CHECKWIN(1);

	/* to avoid destroying twice .. */
	kill_xid(L, win);

	XDestroyWindow(dpy, win);
	return 0;
}

METHOD(win_map)
{
	Window win = CHECKWIN(1);
	XMapWindow(dpy, win);
	return 0;
}

METHOD(win_unmap)
{
	Window win = CHECKWIN(1);
	XMapWindow(dpy, win);
	return 0;
}


/********************************************************
 * GFX
********************************************************/

/* pix,hotx,hoty,w,h = root:read_bitmap_file(name) */
METHOD(g_read_bitmap_file)
{
	Drawable win = CHECKDRAW(1);
	Pixmap pix;
	unsigned w, h;
	int x,y;

	if (XReadBitmapFile(dpy, win, CHECKSTR(2), &w, &h, &pix, &x, &y) != BitmapSuccess)
		return 0;
	MKPIX(pix);
	lua_pushnumber(L, x);
	lua_pushnumber(L, y);
	lua_pushnumber(L, w);
	lua_pushnumber(L, h);
	return 5;
}

METHOD(pix_destroy)
{
	Pixmap pix = CHECKPIX(1);
	kill_xid(L, pix);
	XFreePixmap(dpy,pix);
	return 0;
}

/* win:get_image(x,y,w,h) */
METHOD(g_get_image)
{
	Drawable win = CHECKDRAW(1);
	int x = OPTINT(2, 0);
	int y = OPTINT(3, 0);
	unsigned int w = OPTINT(4, -1);
	unsigned int h = OPTINT(5, -1);
	XImage *ximg;
	FIXWHXY();
	ximg = XGetImage(dpy, win, x, y, w, h, ~0, ZPixmap);
	if (!ximg) return 0;
	newptr(L,ximg, "<image>");
	return 1;
}

/* win:put_image(img,[sx],[sy],[dx],[dy],[w],[h]) */
METHOD(g_put_image)
{
	Drawable win = CHECKDRAW(1);
	XImage *img = CHECKIMG(2);
	int sx = OPTINT(3,0);
	int sy = OPTINT(4,0);
	int dx = OPTINT(5,0);
	int dy = OPTINT(6,0);
	int w = OPTINT(7,img->width);
	int h = OPTINT(8,img->height);
	XPutImage(dpy, win, gc, img, sx, sy, dx, dy, w, h);
	return 0;
}

METHOD(g_create_pixmap)
{
	Drawable win = CHECKDRAW(1);
	unsigned int w = OPTINT(2, -1);
	unsigned int h = OPTINT(3, -1);
	FIXWH();
	return MKPIX(XCreatePixmap(dpy, win, w, h, DefaultDepth(dpy, scr)));
}

/* win:draw_rectangles({...},fill) */
METHOD(g_draw_rectangles)
{
	Drawable win = CHECKDRAW(1);
	int count,i;
	XRectangle *xr;
	lua_settop(L, 2);
	count = lua_objlen(L, 2);
	xr = alloca(sizeof(xr[0]) * count);
	for (i = 1; i <= count; i++) {
		lua_rawgeti(L, -1, i);
		if (lua_isnil(L, -1)) luaL_argerror(L, i, "premature end of rectangle array");
		lua_getfield(L, -1, "x");
		lua_getfield(L, -2, "y");
		lua_getfield(L, -3, "w");
		lua_getfield(L, -4, "h");
		xr[i].x = CHECKINT(-4);
		xr[i].y = CHECKINT(-3);
		xr[i].width = CHECKINT(-2);
		xr[i].height = CHECKINT(-1);
		lua_pop(L, 5);
	}
	if (lua_toboolean(L, 3))
		XFillRectangles(dpy, win, gc, xr, count);
	else
		XDrawRectangles(dpy, win, gc, xr, count);
	return 0;
}

METHOD(g_draw_arcs)
{
	Drawable win = CHECKDRAW(1);
	int count,i;
	XArc *xa;
	lua_settop(L, 2);
	count = lua_objlen(L, 2);
	xa = alloca(sizeof(xa[0]) * count);
	for (i = 1; i <= count; i++) {
		lua_rawgeti(L, -1, i);
		if (lua_isnil(L, -1)) luaL_argerror(L, i, "premature end of arc array");
		lua_getfield(L, -1, "x");
		lua_getfield(L, -2, "y");
		lua_getfield(L, -3, "w");
		lua_getfield(L, -4, "h");
		lua_getfield(L, -5, "a1");
		lua_getfield(L, -6, "a2");
		xa[i].x = CHECKINT(-6);
		xa[i].y = CHECKINT(-5);
		xa[i].width = CHECKINT(-4);
		xa[i].height = CHECKINT(-3);
		xa[i].angle1 = CHECKINT(-2);
		xa[i].angle2 = CHECKINT(-1);
		lua_pop(L, 7);
	}
	if (lua_toboolean(L, 3))
		XFillArcs(dpy, win, gc, xa, count);
	else
		XDrawArcs(dpy, win, gc, xa, count);
	return 0;
}



METHOD(g_draw_segments)
{
	Drawable win = CHECKDRAW(1);
	int count,i;
	XSegment *xs;
	lua_settop(L, 2);
	count = lua_objlen(L, 2);
	xs = alloca(sizeof(xs[0]) * count);
	for (i = 1; i <= count; i++) {
		lua_rawgeti(L, -1, i);
		if (lua_isnil(L, -1)) luaL_argerror(L, i, "premature end of rectangle array");
		lua_getfield(L, -1, "x");
		lua_getfield(L, -2, "y");
		lua_getfield(L, -3, "w");
		lua_getfield(L, -4, "h");
		xs[i].x1 = CHECKINT(-4);
		xs[i].y1 = CHECKINT(-3);
		xs[i].x2 = CHECKINT(-2);
		xs[i].y2 = CHECKINT(-1);
		lua_pop(L, 5);
	}
	XDrawSegments(dpy, win, gc, xs, count);
	return 0;
}


METHOD(g_draw_points)
{
	Drawable win = CHECKDRAW(1);
	int count,i;
	XPoint *xp;
	lua_settop(L, 2);
	count = lua_objlen(L, 2);
	xp = alloca(sizeof(xp[0]) * count);
	for (i = 1; i <= count; i++) {
		lua_rawgeti(L, -1, i);
		if (lua_isnil(L, -1)) luaL_argerror(L, i, "premature end of pixel array");
		lua_getfield(L, -1, "x");
		lua_getfield(L, -2, "y");
		xp[i].x = CHECKINT(-2);
		xp[i].y = CHECKINT(-1);
		lua_pop(L, 3);
	}
	XDrawPoints(dpy, win, gc, xp, count, CHECKBOOL(3));
	return 0;
}

METHOD(g_draw_lines)
{
	Drawable win = CHECKDRAW(1);
	int count,i;
	XPoint *xp;
	lua_settop(L, 2);
	count = lua_objlen(L, 2);
	xp = alloca(sizeof(xp[0]) * count);
	for (i = 1; i <= count; i++) {
		lua_rawgeti(L, -1, i);
		if (lua_isnil(L, -1)) luaL_argerror(L, i, "premature end of lines array");
		lua_getfield(L, -1, "x");
		lua_getfield(L, -2, "y");
		xp[i].x = CHECKINT(-2);
		xp[i].y = CHECKINT(-1);
		lua_pop(L, 3);
	}
	XDrawLines(dpy, win, gc, xp, count, CHECKBOOL(3));
	return 0;
}

/* win:draw_string(str,[x,y]) */
METHOD(g_draw_string)
{
	Drawable win = CHECKDRAW(1);
	size_t l;
	const char *s;
	XChar2b *us;
	int x = OPTINT(3,0);
	int y = OPTINT(4,0);


	if (!lua_istable(L, 2)) {
		s = luaL_checklstring(L, 2, &l);
		XDrawString(dpy, win, gc, x, y, (void*)s, l);
	} else {
		int i;
		l = lua_objlen(L, 2);
	 	us = alloca(sizeof(us[0])*l);
		for (i = 0; i < l; i++) {
			uint16_t n;

			lua_rawgeti(L, 2, i+1);
			n = lua_tonumber(L, -1);
			us[i].byte1 = n>>8;
			us[i].byte2 = n&0xff;
			lua_pop(L, 1);
		}
		XDrawString16(dpy, win, gc, x, y, us, l);
	}

/*	if (global_fontset) {
		DEBUG("using global fontset");
		Xutf8DrawString(dpy, win, global_fontset, gc, x, y, s, l);
	}
	else*/
//	XDrawString(dpy, win, gc, x, y, (void*)s, l);
	return 0;
}

/********************************************************
 * Destructors
 ********************************************************/

/* kill font */
METHOD(font_gc)
{
	myfont_t *f = CHECKFONT(1);
	if (f->set) {
		if (f->set == global_fontset)
			global_fontset = NULL;
		XFreeFontSet(dpy, f->set);
	} else {
		XFree(f->xfont);
	}
	return 0;
}

METHOD(font_info)
{
	myfont_t *f = CHECKFONT(1);
	lua_newtable(L);
	lua_pushnumber(L, f->ascent); lua_setfield(L, -2, "ascent");
	lua_pushnumber(L, f->descent); lua_setfield(L, -2, "descent");
	lua_pushnumber(L, f->height); lua_setfield(L, -2, "height");
	return 1;
}

METHOD(font_width)
{
	myfont_t *f = CHECKFONT(1);
	size_t tl;
	const char *text = luaL_checklstring(L, 2, &tl);
	XRectangle r;

	if (f->set) {
		XmbTextExtents(f->set, text, tl, NULL, &r);
		lua_pushnumber(L, r.width);
	} else lua_pushnumber(L, XTextWidth(f->xfont, text, tl));
	return 1;
}

/* unified destructor for window, pixmap, cursor */
METHOD(gc_xid)
{
	void (*destruct)(Display *,XID);
	XID xid, *x = lua_touserdata(L, 1);

	if (!x) return 0;
	xid = *x;
	*x = 0;

	lua_getfield(L, LUA_REGISTRYINDEX, "destruct");
	lua_rawgeti(L, -1, xid);
	if (lua_isnil(L, -1)) return 0;
	destruct = lua_touserdata(L, -1);
	lua_pop(L, 1); /* pop udata */
	if (destruct) destruct(dpy,xid);
	lua_getfield(L, LUA_REGISTRYINDEX, "xid");
	lua_pushnil(L);
	lua_rawseti(L, -2, xid); /* remove destruct .. */
	lua_rawseti(L, -3, xid); /* and xid */
	return 0;
}

/* basic methods */
#define X(name) { #name, x_##name },
#define WIN(name) { #name, win_##name },
#define FONT(name) { #name, font_##name },
#define PIX(name) { #name, pix_##name },
#define G(name) { #name, g_##name },
#define CUR(name) { #name, cur_##name },

static luaL_reg font_method[] = {
	FONT(info) FONT(width)
	{ "__gc", font_gc },
	{ NULL, NULL }
};

static luaL_reg x_method[] = {
	X(spawn)		X(get_events)		X(geometry)		X(string2keysym)
	X(keysym2string)	X(keysym2keycode)	X(keycode2keysym)	X(modmap)
	X(intern_atom)		X(create_cursor)	X(load_font)		X(color)
	X(fg)			X(bg)			X(set_font)		X(init)
	X(query_tree)		X(sync)			X(flush)
	{ NULL, NULL }
};

static luaL_reg win_method[] = {
	WIN(set_attributes)	WIN(create_window)	WIN(query_pointer)	WIN(list_properties)
	WIN(set_property)	WIN(get_property)	WIN(get_attributes)	WIN(grab_key)
	WIN(grab_button)	WIN(ungrab_key)		WIN(ungrab_button)	WIN(send_message)
	WIN(send_configure)	WIN(configure)		WIN(clear)		WIN(destroy)
	WIN(map)		WIN(unmap)

	G(put_image)		G(get_image)		G(draw_rectangles)	G(draw_arcs)
	G(draw_points)		G(draw_lines)		G(draw_string)
	G(create_pixmap)	G(read_bitmap_file)

	{ "__gc", gc_xid },
	{ NULL, NULL }
};

static	luaL_reg pix_method[] = {
	PIX(destroy)

	G(put_image)		G(get_image)		G(draw_rectangles)	G(draw_arcs)
	G(draw_points)		G(draw_lines)		G(draw_string)		G(create_pixmap)
	G(read_bitmap_file)

	{ "__gc", gc_xid },
	{ NULL, NULL }
	
};

static	luaL_reg cur_method[] = {
	CUR(destroy)		CUR(recolor)
	{ "__gc", gc_xid },
	{ NULL, NULL }
	
};
#undef CUR
#undef G
#undef PIX
#undef WIN
#undef X


void	meta_register(lua_State *L, const char *mname, luaL_reg *meth)
{
	luaL_newmetatable(L, mname);
	if (meth) {
		lua_pushvalue(L, -1);
		lua_setfield(L, -1, "__index");
		luaL_register(L, NULL, meth);
	}
	lua_pushstring(L, mname);
	lua_rawset(L, LUA_REGISTRYINDEX);
//	lua_setfield(L, LUA_REGISTRYINDEX, mname);
}

void	weak_table(lua_State *L, const char *mode)
{
	DEBUG("top=%d",lua_gettop(L));
	/* the table */
	lua_newtable(L);

	/* weak mode metatable */
	lua_newtable(L);
	lua_pushstring(L, mode);
	lua_setfield(L, -2, "__mode");
	lua_setmetatable(L, -2);
	DEBUG("top=%d",lua_gettop(L));
}

int main()
{
	int status;
	lua_State *L = lua_open();
	luaL_openlibs(L);
	luaL_register(L, "x", x_method);
	meta_register(L, "<window>", win_method);
	meta_register(L, "<pixmap>", pix_method);
	meta_register(L, "<cursor>", cur_method);
	meta_register(L, "<font>", font_method);

	/* process table contexes are weak keys (someone has
         * to hold the process handle */
//	weak_table(L, "k");
	lua_newtable(L);
	lua_setfield(L, LUA_REGISTRYINDEX, "<spawn.context>");

	weak_table(L, "v");
	lua_setfield(L, LUA_REGISTRYINDEX, "xid");

	lua_newtable(L);
	lua_setfield(L, LUA_REGISTRYINDEX, "destruct");

	status = luaL_dofile(L, "main.lua");
	if (status && !lua_isnil(L, -1)) {
		const char *msg = lua_tostring(L, -1);
		if (!msg) fprintf(stderr, "unknown error!");
		fprintf(stderr, "%s\n", msg);
	}
	lua_close(L);
	return 0;
}

#if 0
	Display *dpy;
	Window rootwin;
	Window win;
	Colormap cmap;
	XImage *ximg;
	XEvent e;
	int scr,w,h;
	GC gc;
	Pixmap pix;

	if(!(dpy=XOpenDisplay(NULL))) {
		fprintf(stderr, "ERROR: could not open display\n");
		exit(1);
	}
	scr = DefaultScreen(dpy);
	rootwin = RootWindow(dpy, scr);
	cmap = DefaultColormap(dpy, scr);
	win=XCreateSimpleWindow(dpy, rootwin, 1, 1, 100, 50, 0, 
			BlackPixel(dpy, scr), BlackPixel(dpy, scr));
	XStoreName(dpy, win, "hello");
	gc=XCreateGC(dpy, win, 0, NULL);


	pix = XCreatePixmap(dpy, rootwin,
			  w = 1280,
			  h = 720,
			  DefaultDepth(dpy,scr));

	ximg = XGetImage(dpy, rootwin, 0, 0, w, h, ~0, ZPixmap);

	shade_ximage_generic(ximg, 128);
	XPutImage(dpy, pix, gc, ximg, 0,0, 0,0, w,h);
//	XSetWindowBackgroundPixmap(dpy, win, pix);
	XMapWindow(dpy, win);
	XSetForeground(dpy, gc, WhitePixel(dpy, scr));
	XSelectInput(dpy, win, ExposureMask|ButtonPressMask);

while(1) {
		XNextEvent(dpy, &e);
		if(e.type==Expose && e.xexpose.count<1) {
			XCopyArea(dpy, pix, win, gc, 0, 0, 100, 50, 0, 0);
			XDrawString(dpy, win, gc, 10, 10, "Hello World!", 12);
		}
		else if(e.type==ButtonPress) break;
	}

	XCloseDisplay(dpy);
#endif
